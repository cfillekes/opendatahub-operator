---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: superset
  namespace: {{ meta.namespace }}
  labels:
    app: superset
    service: superset

---
apiVersion: v1
kind: ConfigMap
metadata:
  name: superset
  namespace: {{ meta.namespace }}
data:
  FLASK_APP: superset
  SUPERSET_ADM_PWD: {{ SUPERSET_ADMIN_PASSWORD }}
  SUPERSET_CONFIG_PATH: /etc/superset/superset_config.py
  superset_config.py: |
    import os

    MAPBOX_API_KEY = os.getenv('MAPBOX_API_KEY', '')
    SQLALCHEMY_DATABASE_URI = '{{ SUPERSET_SQLALCHEMY_DB_URI }}'
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    SECRET_KEY = os.getenv('SUPERSET_SECRET_KEY','{{ SUPERSET_SECRET_KEY }}')
    DATA_DIR = '/var/lib/superset'

---
apiVersion: apps.openshift.io/v1
kind: DeploymentConfig
metadata:
  name: superset
  namespace: {{ meta.namespace }}
  labels:
    app: superset
spec:
  replicas: 1
  selector:
    deploymentconfig: superset
  template:
    metadata:
      labels:
        app: superset
        deploymentconfig: superset
    spec:
      initContainers:
        - name: superset-init
          envFrom:
            - configMapRef:
                name: superset
          image: docker.io/amancevice/superset:{{ SUPERSET_VERSION }}
          imagePullPolicy: Always
          command:
            - /bin/sh
            - /usr/local/bin/superset-init
            - --username {{ SUPERSET_ADMIN_USER }} --firstname {{ SUPERSET_ADMIN_FNAME }} --lastname {{ SUPERSET_ADMIN_LNAME }} --email {{ SUPERSET_ADMIN_EMAIL }} --password {{ SUPERSET_ADMIN_PASSWORD }}
          volumeMounts:
            - mountPath: /var/lib/superset
              name: superset-data
            - mountPath: /etc/superset
              name: superset-config
      containers:
        - name: superset
          envFrom:
            - configMapRef:
                name: superset
          image: docker.io/amancevice/superset:{{ SUPERSET_VERSION }}
          imagePullPolicy: Always
          ports:
            - containerPort: 8088
              protocol: TCP
          volumeMounts:
            - mountPath: /var/lib/superset
              name: superset-data
            - mountPath: /etc/superset
              name: superset-config
      volumes:
        - name: superset-data
          persistentVolumeClaim:
            claimName: superset-data
        - configMap:
            defaultMode: 420
            items:
              - key: superset_config.py
                path: superset_config.py
            name: superset
          name: superset-config
  triggers:
    - type: ConfigChange

---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: superset-data
  namespace: {{ meta.namespace }}
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: {{ SUPERSET_VOLUME_SIZE }}

---
apiVersion: v1
kind: Service
metadata:
  name: superset
  namespace: {{ meta.namespace }}
  labels:
    app: superset
spec:
  ports:
    - name: 8088-tcp
      port: 8088
      protocol: TCP
      targetPort: 8088
  selector:
    deploymentconfig: superset
  type: ClusterIP

---
apiVersion: route.openshift.io/v1
kind: Route
metadata:
  name: superset
  namespace: {{ meta.namespace }}
  labels:
    app: superset
spec:
  port:
    targetPort: 8088-tcp
  to:
    kind: Service
    name: superset