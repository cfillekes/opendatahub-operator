<img src="datahub_color_vert-wht-bg.png" alt="Open Data Hub, an AI platform powered by Open Source" title="Open Data Hub, an AI platform powered by Open Source" />

Updating the Open Data Hub ClusterServiceVersion
----------

Upon a new release of the Open Data Hub operator, the [Open Data Hub community-operator](https://github.com/operator-framework/community-operators/tree/master/community-operators/opendatahub-operator) in the OpenShift OperatorHub will need to be updated. The [operator-sdk](https://github.com/operator-framework/operator-sdk) will be used to create a new Cluster Service Version(CSV) file from the previous release version

```bash
# Create 4.5.6 release from the previous release version of 1.2.3
$ operator-sdk olm-catalog gen-csv --csv-version 4.5.6 --from-version 1.2.3
```

Based on the example above, a new folder will be created `deploy/olm-catalog/opendatahub-operator/4.5.6` that contains the new clusterserviceversion.yaml file for the 4.5.6 release. This step still doesn't add the owned (or required) CRDs to the new release version folder so make sure you copy the opendatahubs.opendatahub.io CRD to the new folder.

Next you'll need to manually update the below fields to customize them for the current release

```yaml
metadata:
  annotations:
    # List of example ODH CustomResources the user can select from for deploying Open Data Hub via the OpenShift OperatorHub
    alm-examples:[]
spec:
  description: "Description of all the ODH components and features to highlight in this release"
```

Verify the Open Data Hub ClusterServiceVersion Release is formated correctly
----------
The [operator-courier](https://github.com/operator-framework/operator-courier) tool can be used to validate that an Open Data Hub ClusterServiceVersion has all of the required properties necessary for adding it to OpenShift OperatorHub.

```bash
# Install the operator-courier tool
$ pip3 install operator-courier

# operator-courier will process the opendatahub-operator bundle directory and report any errors or warnings
$ operator-courier verify --ui_validate_io deploy/olm-catalog/opendatahub-operator
```

Preview the Open Data Hub ClusterServiceVersion in OperatorHub WebUI
----------
When descriptive text in the ClusterServiceVersion file, you can view a preview of how the file will be rendered in the OpenShift OperatorHub webui using the operatorhub.io [preview tool](https://operatorhub.io/preview). Using this tool, you can copy the contents of a clusterserviceversion.yaml file in this tool and it will render a preview of what this ClusterServiceVersion will look like in the OpenShift OperatorHub webui.


Submitting a new Open Data Hub operator version to OperatorHub community-operators
----------
When a new Open Data Hub operator version is released, the [OperatorHub community operator](https://github.com/operator-framework/community-operators) needs to be updated by submitting a pull request. The only change needed for the pull request, is to mirror the folder [deploy/olm-catalog/opendatahub-operator](deploy/olm-catalog/opendatahub-operator) in the [community-operator/opendatahub-operator](https://github.com/operator-framework/community-operators/tree/master/community-operators/opendatahub-operator) folder in the community-operator repository.


Deploying the Open Data Hub operator ClusterServiceVersion manually to a namespace
----------
To test a new Open Data Hub ClusterServiceVersion in the OpenShift webui, you can deploy it manually to a namespace by creating an OperatorGroup that specifies the namepsace where Operator Lifecycle Manager will deploy the operator and applying the CSV to that namespace which notifies OLM of what CRDs, permissions, service account and deployments the operator requires for a successful install

1. Add any necessary CRDs that are required for any ODH components. As of the v0.5.0 release, this would include CRDs for the following ODH components: Argo, Seldon, Strimzi Kafka

1. Creating a new namespace
   ```bash
   $ oc new-project test-odh-csv
   ```

1. If you are including Kafka in your ODH deployment, you'll need to deploy the Strimzi Kafka operator via the OpenShift OperatorHub webui --OR-- manually by following the instructions outlined in [Deploying Kafka Setup](docs/deploying-kafka.adoc)

1. Create the RBAC for the odh operator. These RBAC steps would normally be performed by OLM in the target namespace
   ```bash
   # Create the ODH operator service account
   $ oc apply -f deploy/service_account.yaml

   # These permissions must match the rules in the CSV you are testing
   # CSV permissions are listed at .spec.install.spec.permissions in the yaml file
   $ oc apply -f deploy/role.yaml

   $ oc apply -f deploy/role_binding.yaml
   ```

1. Create an Operator Group to specify what namespaces the operator controls
   ```yaml
   apiVersion: operators.coreos.com/v1alpha2
   kind: OperatorGroup
   metadata:
     name: opendatahub-operator
   spec:
    targetNamespaces:
    - test-odh-csv
   ```

1. Create the ODH CSV
   ```bash
   # Replace the default CSV namespace "placeholder" with the target namespace (test-odh-csv)
   $ sed -i 's/placeholder/test-odh-csv' deploy/olm-catalog/opendatahub-operator/1.2.3/opendatahub-operator.v1.2.3.clusterserviceversion.yaml
   $ oc apply -f deploy/olm-catalog/opendatahub-operator/1.2.3/opendatahub-operator.v1.2.3.clusterserviceversion.yaml
   ```

1. Check the status of the CSV object to confirm that it was installed successfully
   ```bash
   # X.Y.Z is the version number of the operator you are testing
   $ oc get csv opendatahub-operator.vX.Y.Z
   ```

1. Confirm that the opendatahub-operator pod is running
   ```bash
   $ oc get pods
   ```

Once the CSV is installed, you can login to the OpenShift console webUI, select your project `test-odh-csv` and navigate to `Catalog->Installed Operators`.  On this page, you can click on `Open Data Hub Operator` to view a description of ODH and deploy ODH by clicking on `Create New`


Other Resources
----------
- [Generating a Cluster Service Version with Operator SDK](https://github.com/operator-framework/operator-sdk/blob/master/doc/user/olm-catalog/generating-a-csv.md)
- [OLM ClusterServiceVersion](https://github.com/operator-framework/operator-lifecycle-manager/blob/master/doc/design/building-your-csv.md)
- [OLM OperatorGroup](https://github.com/operator-framework/operator-lifecycle-manager/blob/master/doc/design/operatorgroups.md)
